package main

import "github.com/jroimartin/gocui"

// Layout creates chat ui
func Layout(g *gocui.Gui) error {
    maxX, maxY := g.Size()
    g.Cursor = true

    if messages, err := g.SetView("messages", 0, 0, maxX-31, maxY-5); err != nil {
        if err != gocui.ErrUnknownView {
            return err
        }
        messages.Title = " messages: "
        messages.Autoscroll = true
        messages.Wrap = true
    }

    if input, err := g.SetView("input", 0, maxY-5, maxX-31, maxY-1); err != nil {
        if err != gocui.ErrUnknownView {
            return err
        }
        input.Title = " send: "
        input.Autoscroll = false
        input.Wrap = true
        input.Editable = true
    }

    if peers, err := g.SetView("peers", maxX-30, maxY/2, maxX-1, maxY-1); err != nil {
        if err != gocui.ErrUnknownView {
            return err
        }
        peers.Title = " peers: "
        peers.Autoscroll = true
        peers.Wrap = true
    }

    if stats, err := g.SetView("stats", maxX-30, 0, maxX-1, maxY/2-1); err != nil {
        if err != gocui.ErrUnknownView {
            return err
        }
        stats.Title = " stats: "
        stats.Autoscroll = true
        stats.Wrap = true
    }

    if topic, err := g.SetView("topic", maxX/2-10, maxY/2-1, maxX/2+10, maxY/2+1); err != nil {
        if err != gocui.ErrUnknownView {
            return err
        }
        g.SetCurrentView("topic")
        topic.Title = " topic: "
        topic.Autoscroll = false
        topic.Wrap = true
        topic.Editable = true
    }

    if password, err := g.SetView("password", maxX/2-10, maxY/2-1, maxX/2+10, maxY/2+1); err != nil {
        if err != gocui.ErrUnknownView {
            return err
        }
        g.SetCurrentView("password")
        password.Title = " password: "
        password.Autoscroll = false
        password.Wrap = true
        password.Editable = true
    }

    if name, err := g.SetView("name", maxX/2-10, maxY/2-1, maxX/2+10, maxY/2+1); err != nil {
        if err != gocui.ErrUnknownView {
            return err
        }
        g.SetCurrentView("name")
        name.Title = " name: "
        name.Autoscroll = false
        name.Wrap = true
        name.Editable = true
    }
    return nil
}