package main

import (
    "fmt"
    "time"

    "github.com/jroimartin/gocui"
    "github.com/ethereum/go-ethereum/p2p"
    "github.com/ethereum/go-ethereum/p2p/discover"
    "github.com/ethereum/go-ethereum/whisper/whisperv6"

)

var (
    name            string
    password        string
    stringTopic     []byte
    symKey          []byte
    whisperTopic    whisperv6.TopicType
    srv             p2p.Server
    whisper         *whisperv6.Whisper
    filter          whisperv6.Filter
)

// Disconnect from chat and close
func Disconnect(g *gocui.Gui, v *gocui.View) error {
    whisper.Stop()
    return gocui.ErrQuit
}

// Send message
func Send(g *gocui.Gui, v *gocui.View) error {
    payload := v.Buffer()

    params := whisperv6.MessageParams{
        TTL: uint32(25000),
        KeySym: symKey,
        WorkTime: uint32(10),
        Topic: whisperTopic,
        Payload: []byte(payload),
    }
    newMsg, _ := whisperv6.NewSentMessage(&params)
    newEnv, _ := newMsg.Wrap(&params)
    _ = whisper.Send(newEnv)

    g.Update(func(g *gocui.Gui) error {
        v.Clear()
        v.SetCursor(0, 0)
        v.SetOrigin(0, 0)
        return nil
    })
    return nil
}

func msgLoop(g *gocui.Gui) {

    messagesView, _ := g.View("messages")
    peersView, _ := g.View("peers")
    statsView, _ := g.View("stats")
    go func() {
        for {
            time.Sleep(250 * time.Millisecond)
            msgs := filter.Retrieve()
            for j := 0; j < len(msgs); j++ {
                newmsg := msgs[j]
                valid := newmsg.ValidateAndParse()
                if valid == true {
                    pl := newmsg.Payload
                    payload := string(pl[:len(pl)])
                    fmt.Fprintln(messagesView,
                        "<", newmsg.Sent, ">(", newmsg.Signature, "):",
                        payload)
                }
            }
            g.Update(func(g *gocui.Gui) error {
                infos := parsePeers(srv.PeersInfo())
                fmt.Fprintln(peersView, infos)
                fmt.Fprintln(statsView, whisper.Stats())
                return nil
            })
        }
    }()
}

func parsePeers(peers []*p2p.PeerInfo) string {
    infoStr := ""
    for i := 0; i < len(peers); i++ {
        infoStr = infoStr + ("ID: "+peers[i].ID+"\n")
    }
    return infoStr
}


func SetName(g *gocui.Gui, v *gocui.View) error {
    name = v.Buffer();
    g.SetViewOnTop("password")
    g.SetCurrentView("password")
    return nil
}

func SetPassword(g *gocui.Gui, v *gocui.View) error {
    password = v.Buffer();
    g.SetViewOnTop("topic")
    g.SetCurrentView("topic")
    return nil
}


func SetTopic(g *gocui.Gui, v *gocui.View) error {
    stringTopic = []byte(v.Buffer())
    return Connect(g, v)
}


func Connect(g *gocui.Gui, v *gocui.View) error {

    nodes := []*discover.Node{
        discover.MustParseNode("enode://90cbf961c87eb837adc1300a0a6722a57134d843f0028a976d35dff387f101a2754842b6b694e50a01093808f304440d4d968bcbc599259e895ff26e5a1a17cf@51.15.194.39:30303"),
        discover.MustParseNode("enode://fa63a6cc730468c5456eab365b2a7a68a166845423c8c9acc363e5f8c4699ff6d954e7ec58f13ae49568600cff9899561b54f6fc2b9923136cd7104911f31cce@163.172.168.202:30303"),    
    }
    
    whisper = whisperv6.New(nil)
    keyID, _ := whisper.NewKeyPair()
    privKey, _ := whisper.GetPrivateKey(keyID)
    
    srvConfig := p2p.Config{
        NoDiscovery: false,
        MaxPeers:   10,
        BootstrapNodes: nodes,
        StaticNodes: nodes,
        TrustedNodes: nodes,
        ListenAddr: "localhost:0",
        Protocols: []p2p.Protocol{whisper.Protocols()[0]},
        PrivateKey: privKey,
    }

    srv = p2p.Server{
        Config: srvConfig,
    }
    _ = whisper.SetMinimumPoW(0.0)

    
    symKeyId, _ := whisper.AddSymKeyFromPassword(password)
    symKey, _ = whisper.GetSymKey(symKeyId)
    whisperTopic = whisperv6.BytesToTopic([]byte(stringTopic))
    filter = whisperv6.Filter{
        KeySym: symKey,
        Topics: [][]byte{[]byte(stringTopic)},
    }
    whisper.Subscribe(&filter)

    
    srv.Start()
    whisper.Start(&srv)

    g.SetViewOnTop("messages")
    g.SetViewOnTop("peers")
    g.SetViewOnTop("stats")
    g.SetViewOnTop("input")
    g.SetCurrentView("input")

    msgLoop(g)

    return nil
}