up:
	docker-compose up -d
	docker-compose exec node_a go run node/*.go
	docker-compose stop
geth:
	docker-compose up -d
	docker-compose exec -u eth_user node_a geth init /home/eth_user/eth_common/rinkeby.json
	docker-compose exec -d -u eth_user node_a geth --shh --rpc --networkid=4 --datadir=$HOME/.rinkeby --syncmode=light --ethstats='123flflnnod:Respect my authoritah!@stats.rinkeby.io' --bootnodes=enode://90cbf961c87eb837adc1300a0a6722a57134d843f0028a976d35dff387f101a2754842b6b694e50a01093808f304440d4d968bcbc599259e895ff26e5a1a17cf@51.15.194.39:30303
	docker-compose exec node_a go run node/node.go
build:
	docker-compose build
down:
	docker-compose down
