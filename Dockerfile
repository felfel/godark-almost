FROM ubuntu:16.04

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install --yes software-properties-common curl
RUN add-apt-repository ppa:ethereum/ethereum
RUN add-apt-repository ppa:gophers/archive
RUN apt-get update && apt-get install --yes geth git golang-1.10-go

USER root
ENV GOPATH=/home/go/
ENV PATH="$PATH:/usr/lib/go-1.10/bin"
WORKDIR /home/go/src/app
COPY . .

RUN bash get.sh

RUN go get -d -v ./...
RUN go install -v ./...

ENTRYPOINT bash